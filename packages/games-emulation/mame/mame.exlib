# Copyright 2010-2011 Elias Pipping <pipping@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

MY_PV=$(ever delete_all $(ever range 1-2))

if ever is_scm ; then
    require github [ user='mamedev' ]
else
    export_exlib_phases src_unpack
    DOWNLOADS="
        https://github.com/mamedev/mame/releases/download/${PN}${MY_PV}/${PN}${MY_PV}s.zip
    "
fi

require lua [ whitelist=5.3 multibuild=false ]

export_exlib_phases src_compile src_install

SUMMARY="Multiple Arcade Machine Emulator"
DESCRIPTION="
When used in conjunction with images of the original arcade game's ROM and disk data, MAME attempts
to reproduce that game as faithfully as possible on a more modern general-purpose computer. MAME can
currently emulate several thousand different classic arcade video games from the late 1970s through
the modern era.
"
HOMEPAGE="https://mamedev.org"

LICENCES="GPL-2"
SLOT="0"
MYOPTIONS="
    platform: amd64 x86

    ( providers: ijg-jpeg jpeg-turbo ) [[ number-selected = exactly-one ]]
"

# so far there are no tests defined, but the makefile calls
# unprefixed gcc anyway, so restrict until there are tests.
RESTRICT="test"

# build+run:
#     dev-cpp/asio
# https://github.com/mamedev/mame/issues/5721
DEPENDENCIES="
    build:
        app-arch/unzip
        dev-lang/python:*[>=3]
        virtual/pkg-config
    build+run:
        dev-libs/expat
        dev-libs/glm
        dev-libs/libglvnd
        dev-libs/pugixml
        dev-libs/utf8proc
        dev-db/sqlite
        media-libs/SDL:2[X]
        media-libs/SDL_ttf:2
        media-libs/flac:=
        media-libs/fontconfig
        media-libs/portaudio
        media-libs/portmidi
        sys-libs/zlib
        sys-sound/alsa-lib
        x11-dri/glu
        x11-libs/libX11
        x11-libs/libXi
        x11-libs/libXinerama
        x11-libs/qtbase:5
        providers:ijg-jpeg? ( media-libs/jpeg:= )
        providers:jpeg-turbo? ( media-libs/libjpeg-turbo )
"

DEFAULT_SRC_PREPARE_PATCHES=(
    "${FILES}"/${PN}-0.215-genie-systemlua.patch
)

if ! ever is_scm; then
    WORK="${WORKBASE}"

    mame_src_unpack() {
        default

        # exclude bundled dependencies
        # '3rdparty/asio/*' \
        edo unzip -qo ./mame.zip -x \
            '3rdparty/expat/*' \
            '3rdparty/glm/*' \
            '3rdparty/libflac/*' \
            '3rdparty/libjpeg/*' \
            '3rdparty/lua/*' \
            '3rdparty/portaudio/*' \
            '3rdparty/portmidi/*' \
            '3rdparty/pugixml/*' \
            '3rdparty/SDL2/*' \
            '3rdparty/SDL2-override/*' \
            '3rdparty/sqlite3/*' \
            '3rdparty/utf8proc/*' \
            '3rdparty/zlib*' \
            '3rdparty/genie/src/host/lua-5.3.0/*'

        edo rm -f mame.zip
    }
fi

mame_src_compile() {
    local args=(
        USE_BUNDLED_LIB_SDL2=0
        USE_SYSTEM_LIB_ASIO=0
        USE_SYSTEM_LIB_EXPAT=1
        USE_SYSTEM_LIB_FLAC=1
        USE_SYSTEM_LIB_GLM=1
        USE_SYSTEM_LIB_JPEG=1
        USE_SYSTEM_LIB_LUA=1
        USE_SYSTEM_LIB_PORTAUDIO=1
        USE_SYSTEM_LIB_PORTMIDI=1
        USE_SYSTEM_LIB_PUGIXML=1
        USE_SYSTEM_LIB_RAPIDJSON=0
        USE_SYSTEM_LIB_SQLITE3=1
        USE_SYSTEM_LIB_UTF8PROC=1
        USE_SYSTEM_LIB_ZLIB=1

        NOWERROR=1

        AR=${AR}
        CC=${CC}
        CXX=${CXX}
        LD=${CXX}
        PKG_CONFIG=${PKG_CONFIG}
        PYTHON_EXECUTABLE=python3
    )

    emake ${args[@]}
}


mame_src_install() {
    dodoc docs/LICENSE

    local mamebin
    if option platform:amd64; then
        mamebin=mame64
    elif option platform:x86; then
        mamebin=mame32
    fi
    newbin ${mamebin} mame
}

