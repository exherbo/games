# Copyright 2018-2024 Timo Gurr <tgurr@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github [ user=FeralInteractive release=${PV} suffix=tar.xz ] \
    meson \
    systemd-service

SUMMARY="Daemon and library that allows games to request optimisations to be temporarily applied"
DESCRIPTION="
GameMode was designed primarily as a stop-gap solution to problems with the Intel and AMD CPU
powersave or ondemand governors, but is now able to launch custom user defined plugins, and is
intended to be expanded further, as there are a wealth of automation tasks one might want to apply.
"

LICENCES="BSD-3"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS="
    ( providers: elogind systemd ) [[
        *description = [ Login daemon provider ]
        number-selected = exactly-one
    ]]
"

DEPENDENCIES="
    build:
        virtual/pkg-config
    build+run:
        dev-libs/inih
        providers:systemd? ( sys-apps/systemd )
        providers:elogind? ( sys-auth/elogind )
        sys-apps/dbus[>=1.0]
    run:
        sys-auth/polkit:1
    suggestion:
        group/games [[
            description = [ Allow renicing as an unpriviledged user being part of the games group ]
        ]]
"

MESON_SRC_CONFIGURE_PARAMS=(
    -Dwith-dbus-service-dir=/usr/share/dbus-1/services
    -Dwith-examples=false
    -Dwith-pam-limits-dir=/etc/security/limits.d
    -Dwith-pam-renicing=true
    -Dwith-privileged-group=games
    -Dwith-systemd-group=false
    -Dwith-systemd-group-dir=/usr/$(exhost --target)/lib/sysusers.d
    -Dwith-systemd-user-unit=true
    -Dwith-systemd-user-unit-dir=${SYSTEMDUSERUNITDIR}
    -Dwith-util=true
)
MESON_SRC_CONFIGURE_OPTIONS=(
    'providers:systemd -Dwith-sd-bus-provider=systemd'
    'providers:elogind -Dwith-sd-bus-provider=elogind'
)

src_install() {
    meson_src_install

    insinto /etc
    doins example/gamemode.ini
}

