# Copyright 2009, 2010 Ingmar Vanhassel
# Copyright 2014 Alexander Scheuermann
# Copyright 2021 Heiko Becker <heirecka exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require cmake gtk-icon-cache

SUMMARY="An open source clone of Transport Tycoon Deluxe"
HOMEPAGE="https://www.openttd.org/"
DOWNLOADS="https://cdn.openttd.org/${PN}-releases/${PV}/${PNV}-source.tar.xz"

UPSTREAM_DOCUMENTATION="https://wiki.openttd.org/Newbies [[ description = [ Tutorial ] lang = en ]]"

LICENCES="GPL-2"
SLOT="0"
PLATFORMS="~amd64 ~x86"

MYOPTIONS="
    dedicated [[ description = [ Build a dedicated server only binary ] ]]
    doc
"

DEPENDENCIES="
    build:
        virtual/pkg-config
        doc? ( app-doc/doxygen )
    build+run:
        app-arch/lzo:2
        app-arch/xz[>=5.0]
        dev-games/grfcodec
        media-libs/libpng:=[>=1.2]
        net-misc/curl
        sys-libs/zlib
        !dedicated? (
            dev-libs/icu:=[>=3.6.0]
            media-libs/fontconfig[>=2.3.0]
            media-libs/freetype:2
            media-libs/SDL:2[>=2.0][X]
            media-sound/fluidsynth
            x11-libs/harfbuzz
        )
    test:
        dev-cpp/catch
    suggestion:
        !dedicated? ( media-sound/timidity++ [[ description = [ Required for midi playback ] ]] )
    recommendation:
        !dedicated? ( games-simulation/openttd-opengfx[>=0.5] [[ description = [ OpenTTD requires atleast one graphics set to work ] ]] )
"

DEFAULT_SRC_PREPARE_PATCHES=(
    "${FILES}"/${PNV}-Fix-build-with-icu-76-where-icu-i18n-and-icu-uc-beco.patch
    "${FILES}"/${PNV}-Codefix-Compilation-with-GCC-15-due-to-missing-CRTP-.patch
)

CMAKE_SRC_CONFIGURE_PARAMS=(
    -DCMAKE_INSTALL_BINDIR="bin"
    -DCMAKE_INSTALL_DATADIR="/usr/share"
    -DCMAKE_INSTALL_DOCDIR="/usr/share/doc/${PNVR}"
    -DCMAKE_DISABLE_FIND_PACKAGE_Allegro:BOOL=TRUE
    # Would create minidumps on crash, but it isn't fond of releases and
    # apparently openttd uses it via vcpkg
    -DCMAKE_DISABLE_FIND_PACKAGE_unofficial-breakpad:BOOL=TRUE
    -DOPTION_PACKAGE_DEPENDENCIES:BOOL=FALSE
    -DOPTION_USE_ASSERTS:BOOL=FALSE
)
CMAKE_SRC_CONFIGURE_OPTION_DISABLE_FINDS=(
    'doc Doxygen'
)
CMAKE_SRC_CONFIGURE_OPTIONS=(
    'dedicated OPTION_DEDICATED'
)

src_compile() {
    cmake_src_compile

    option doc && ecmake_build --target docs
}

src_install() {
    cmake_src_install

    if option doc ; then
        edo pushd docs/source
        dodoc -r html
        edo popd
    fi
}

