# Copyright 2012 Benedikt Morbach <benedikt.morbach@googlemail.com>
# Distributed under the terms of the GNU General Public License v2

require github [ tag=v${PV} ]
require cmake gtk-icon-cache python [ blacklist=2 multibuild=false ]
require utf8-locale

SUMMARY="A portable open-source implementation of Bioware's Infinity Engine"
DESCRIPTION="
GemRB is a portable open-source implementation of Bioware's Infinity Engine. It was written to
support pseudo-3D role playing games based on the Dungeons & Dragons ruleset, such as the Baldur's
Gate and Icewind Dale series and Planescape: Torment.
"
HOMEPAGE+=" https://gemrb.org"

LICENCES="|| ( GPL-2 GPL-3 )"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS=""

DEPENDENCIES="
    build+run:
        dev-libs/libglvnd
        media-libs/freetype:2
        media-libs/libpng:=
        media-libs/libvorbis
        media-libs/openal [[ note = [ preferred, could also use SDL_mixer:2 as lighter alternative ] ]]
        media-libs/SDL:2[X]
        sys-libs/zlib
    test:
        dev-cpp/gtest
"

DEFAULT_SRC_PREPARE_PATCHES=(
    "${FILES}"/cca8e711247ae67921a1c91ef24ea78415cbea78.patch
)

pkg_setup() {
    # Required for test: StringTest.StringToUpper
    require_utf8_locale
}

src_configure() {
    local cmakeparams=(
        -DCMAKE_BUILD_TYPE:STRING=Release
        -DDATA_DIR:PATH=/usr/share/${PN}
        -DDOC_DIR:PATH=/usr/share/doc/${PNVR}
        -DICON_DIR:PATH=/usr/share/pixmaps
        -DMAN_DIR:PATH=/usr/share/man/man6
        -DMENU_DIR:PATH=/usr/share/applications
        -DMETAINFO_DIR:PATH=/usr/share/metainfo
        -DSVG_DIR:PATH=/usr/share/icons/hicolor/scalable/apps
        -DSYSCONF_DIR:PATH=/etc/${PN}
        -DDISABLE_WERROR:BOOL=TRUE
        -DOPENGL_BACKEND:STRING=OpenGL
        -DPYTHON_VERSION:STRING=$(python_get_abi)
        -DSANITIZE:STRING=None
        -DSDL_BACKEND:STRING=SDL2
        -DUSE_FREETYPE:BOOL=TRUE
        -DUSE_LIBVLC:BOOL=FALSE
        -DUSE_OPENAL:BOOL=TRUE
        -DUSE_PNG:BOOL=TRUE
        -DUSE_SDL_CONTROLLER_API:BOOL=TRUE
        -DUSE_SDLMIXER:BOOL=FALSE
        -DUSE_TESTS:BOOL=$(expecting_tests TRUE FALSE)
        -DUSE_TRACY:BOOL=FALSE
        -DUSE_VORBIS:BOOL=TRUE
    )

    ecmake "${cmakeparams[@]}"
}

src_prepare() {
    cmake_src_prepare

    # fix python shebang
    edo sed \
        -e 's:/usr/bin/python:/usr/bin/python3:g' \
        -i admin/extend2da.py
}

