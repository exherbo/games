# Copyright 2016 Kylie McClain <somasis@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

MY_PN=${PN,,}

require github [ tag=v${PV} ]
require gtk-icon-cache

SUMMARY="A rewrite of the scrolling shooter by Epic MegaGames, originally for DOS"
DOWNLOADS+=" https://www.camanis.net/tyrian/tyrian21.zip"

# The licence of the data is a bit unclear, see
# https://github.com/opentyrian/opentyrian/issues/18 for details.
LICENCES="
    GPL-2
    freedist    [[ note = [ Tyrian original data is not GPL ] ]]
"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS=""

DEPENDENCIES="
    build:
        virtual/unzip
        virtual/pkg-config
    build+run:
        media-libs/SDL:2
        media-libs/SDL_net:2
"

WORK="${WORKBASE}"/${MY_PN}-${PV}

DEFAULT_SRC_COMPILE_PARAMS=(
    # Replaces git describe --tags, which obviously doesn't work in a tarball
    VCS_IDREV="echo v${PV}"
    WITH_NETWORK=true
    # Expects /usr/share/games/tyrian otherwise
    TYRIAN_DIR="/usr/share/${MY_PN}/data"
)

DEFAULT_SRC_INSTALL_PARAMS=(
    prefix=/usr
    exec_prefix=/usr/$(exhost --target)
    docdir=/usr/share/doc/${PNVR}
)

src_install() {
    default

    insinto /usr/share/${MY_PN}
    edo mv "${WORKBASE}"/tyrian21 "${IMAGE}"/usr/share/${MY_PN}/data
    edo chmod -R 644 "${IMAGE}"/usr/share/${MY_PN}/data/*
    edo chmod 755 "${IMAGE}"/usr/share/${MY_PN}/data

    emagicdocs
}

