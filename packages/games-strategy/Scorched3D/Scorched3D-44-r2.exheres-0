# Copyright 2009 Ingmar Vanhassel
# Distributed under the terms of the GNU General Public License v2

require autotools [ supported_autoconf=[ 2.7 ] supported_automake=[ 1.16 ] ]
require wxwidgets

SUMMARY="A modernization of the classic DOS game Scorched Earth, 'The Mother Of All Games', on which it is based"
DESCRIPTION="
Scorched 3D is a simple turn-based artillery game and also a real-time strategy
game in which players can counter each others' weapons with other creative
accessories, shields and tactics. Learn skill in timing, aiming and judgment,
move around on the landscape, or even out-smart your opponent economically.

Scorched 3D incorporates a lively three dimensional landscape that includes
animated jets, naval vessels, water, and even birds, in addition to detailed
tanks and projectiles. Other enhancements include LAN and internet play and
music. Scorched 3D is totally free to play and is available for both Microsoft
Windows and Unix operating systems.
"
HOMEPAGE="http://www.scorched3d.co.uk/"
DOWNLOADS="mirror://sourceforge/${PN,,}/${PNV}-src.tar.gz"

LICENCES="|| ( GPL-3 GPL-2 )"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="
    ( providers: ijg-jpeg jpeg-turbo ) [[ number-selected = exactly-one ]]
"

# FIXME: Internal copy of expat, liblua, dejavu fonts, possibly more
DEPENDENCIES="
    build:
        virtual/pkg-config[>=0.9.0]
    build+run:
        dev-libs/expat
        dev-libs/libglvnd
        media-libs/freealut
        media-libs/freetype:2
        media-libs/glew
        media-libs/libogg
        media-libs/libpng:=
        media-libs/libvorbis
        media-libs/openal
        media-libs/SDL:0[>=1.2.5]
        media-libs/SDL_net:0
        sci-libs/fftw
        sys-libs/zlib
        x11-dri/freeglut
        x11-dri/glu
        providers:ijg-jpeg? ( media-libs/jpeg:= )
        providers:jpeg-turbo? ( media-libs/libjpeg-turbo )
"

WORK=${WORKBASE}/scorched

DEFAULT_SRC_PREPARE_PATCHES=(
    "${FILES}"/${PN}-44-pkgconfig.patch
    "${FILES}"/${PN}-44-fixups.patch
    "${FILES}"/${PN}-44-gcc43.patch
    "${FILES}"/${PN}-44-odbc.patch
    "${FILES}"/${PN}-44-win32.patch
    "${FILES}"/${PN}-43.3d-fix-freetype-includes.patch
    "${FILES}"/${PN}-44-jpeg9.patch
    "${FILES}"/${PN}-44-fix-cpp14.patch
    "${FILES}"/${PN}-44-wxgtk.patch
    "${FILES}"/${PN}-44-freetype.patch
)

src_configure() {
    local myconf=(
        --datadir=/usr/share/${PN,,}
        --disable-serveronly
        --with-docdir=/usr/share/doc/${PNVR}
        --with-wx-config=$(wxwidgets_get_config)
        --without-mysql
        --without-pgsql
    )

    econf "${myconf[@]}"
}

src_install() {
    default

    # install desktop file
    insinto /usr/share/applications
    hereins ${PN,,}.desktop <<EOF
[Desktop Entry]
Name=Scorched 3D
Version=1.1
Exec=${PN,,}
Comment=A game based on the classic DOS game Scorched Earth
Icon=/usr/share/pixmaps/${PN,,}.bmp
Type=Application
Terminal=false
StartupNotify=true
Categories=Game;ActionGame;StrategyGame;
EOF

    # install icon
    insinto /usr/share/pixmaps
    newins data/images/tank-old.bmp ${PN,,}.bmp
}

