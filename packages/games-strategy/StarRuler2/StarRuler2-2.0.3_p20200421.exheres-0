# Copyright 2018-2023 Timo Gurr <tgurr@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github [ user=BlindMindStudios project=${PN}-Source tag=beec9bff697ffbebafaeb66d0cba1856a02cb6db ] \
    freedesktop-desktop \
    gtk-icon-cache

SUMMARY="4X Space Strategy game Star Ruler 2"
DESCRIPTION="
Star Ruler 2 is a massive scale 4X/RTS set in space. Explore dozens, hundreds, or even thousands of
systems in a galaxy of your choosing, expand across its planets, exploit the resources you find,
and ultimately exterminate any who stand in your way. The fate of your empire depends on your
ability to master the economy, field a military, influence galactic politics, and learn what you
can about the universe.
"
HOMEPAGE+=" http://starruler2.com"

LICENCES="
    CCPL-Attribution-NonCommercial-2.0 [[ note = [ Contained art assets, images and 3d models ] ]]
    MIT
"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS="
    ( providers: graphicsmagick imagemagick ) [[
        number-selected = at-least-one
    ]]
"

# doesn't have tests
RESTRICT="test"

DEPENDENCIES="
    build:
        sys-devel/cmake
        providers:graphicsmagick? ( media-gfx/GraphicsMagick[imagemagick] )
        providers:imagemagick? ( media-gfx/ImageMagick )
    build+run:
        media-libs/freetype:2
        media-libs/glew
        media-libs/libpng:=
        media-libs/libvorbis
        media-libs/openal
        net-misc/curl
        sys-libs/zlib
        x11-dri/glu
        x11-dri/mesa [[ note = [ provides libGL ] ]]
        x11-libs/libX11
        x11-libs/libXi
        x11-libs/libXrandr
        x11-libs/libXxf86vm
"

DEFAULT_SRC_PREPARE_PATCHES=(
    "${FILES}"/StarRuler2-fix-linking.patch
)

pkg_setup() {
    exdirectory --allow /opt
}

src_prepare() {
    default

    # fix cross
    # gcc-ar/ranlib are required for liblto_plugin.so. To disable LTO we could
    # also pass NLTO=1 to the emake call instead.
    # general build
    edo sed \
        -e "s:gcc-ar:$(exhost --tool-prefix)gcc-ar:g" \
        -e "s:gcc-ranlib:$(exhost --tool-prefix)gcc-ranlib:g" \
        -e "s:CC=gcc:CC=${CC}:g" \
        -e "s:g++:${CXX}:g" \
        -i source/linux/build.sh
    # angelscript library
    edo sed \
        -e "s:gcc-ranlib:$(exhost --tool-prefix)ranlib --plugin=liblto_plugin.so:g" \
        -i source/angelscript/projects/gnuc/makefile

    # do not chmod
    edo sed \
        -e '/chmod/d' \
        -i StarRuler2.sh

    # create desktop icon
    edo convert sr2.ico sr2.png
}

src_compile() {
     # to disable LTO pass NLTO=1
     emake \
         AR=$(exhost --tool-prefix)gcc-ar \
         RANLIB=$(exhost --tool-prefix)gcc-ranlib \
         -f source/linux/Makefile compile
}

src_install() {
    exeinto /opt/starruler2
    newexe ${PN}.sh ${PN}

    exeinto /opt/starruler2/bin/lin64
    doexe bin/lin64/${PN}.bin

    insinto /opt/starruler2
    doins -r data locales maps mods scripts

    herebin ${PN} <<EOF
#!/bin/sh
exec /opt/starruler2/${PN} $@
EOF

    insinto /usr/share/icons/hicolor/32x32/apps
    newins sr2.png ${PN}.png

    insinto /usr/share/applications
    hereins ${PN}.desktop <<EOF
[Desktop Entry]
Name=${PN}
Version=1.1
Exec=${PN}
Comment=4X Space Strategy game
Icon=${PN}
Type=Application
Terminal=false
StartupNotify=true
Categories=Game;StrategyGame;
EOF
}

pkg_postinst() {
    freedesktop-desktop_pkg_postinst
    gtk-icon-cache_pkg_postinst
}

pkg_postrm() {
    freedesktop-desktop_pkg_postrm
    gtk-icon-cache_pkg_postrm
}

