# Copyright 2024-2025 Timo Gurr <tgurr@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

MY_PNV=NexusMods.App-${PV}-1.linux-x64

require github [ user=Nexus-Mods project=NexusMods.App release=v${PV} suffix=zip ]
#    freedesktop-desktop

SUMMARY="The Nexus Mods app"

DOWNLOADS+="
    https://raw.githubusercontent.com/Nexus-Mods/NexusMods.App/v${PV}/src/NexusMods.App/com.nexusmods.app.metainfo.xml -> ${PNV}.metainfo.xml
    https://raw.githubusercontent.com/Nexus-Mods/NexusMods.App/v${PV}/src/NexusMods.App/icon.svg -> ${PNV}.svg
"

LICENCES="GPL-3"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS=""

RESTRICT="strip"

DEPENDENCIES="
    run:
        app-arch/zstd
    suggestion:
        app-emulation/protontricks [[
            description = [ Required to install Cyberpunk 2077 REDmods ]
        ]]
"

WORK=${WORKBASE}

pkg_setup() {
    exdirectory --allow /opt
}

src_test() {
    :
}

src_install() {
    edo rm uninstall-helper.ps1
    edo rm -rf runtimes/{osx,win}-x64

    insinto /opt/${PN}
    doins -r *

    insinto /usr/share/applications
    hereins com.nexusmods.app.desktop.desktop <<EOF
[Desktop Entry]
Version=1.1
Name=Nexus Mods app
Exec=NexusMods.App %U
Comment=A mod installer, creator and manager for all your popular games
Icon=com.nexusmods.app
Type=Application
Terminal=false
StartupWMClass=NexusMods.App
Categories=Game;Application;
EOF
# adding the mime type causes the login process to fail, last checked: 0.5.2
#MimeType=x-scheme-handler/nxm;
#EOF

    insinto /usr/share/metainfo
    newins "${FETCHEDDIR}"/${PNV}.metainfo.xml com.nexusmods.app.metainfo.xml

    insinto /usr/share/icons/hicolor/scalable/apps
    newins "${FETCHEDDIR}"/${PNV}.svg com.nexusmods.app.svg

    dodir /usr/$(exhost --target)/bin
    dosym /opt/${PN}/NexusMods.App /usr/$(exhost --target)/bin/NexusMods.App

    edo chmod 0755 "${IMAGE}"/opt/${PN}/{NexusMods.App,*.so}
    edo chmod 0755 "${IMAGE}"/opt/${PN}/runtimes/linux-x64/native/7zz
}

#pkg_postinst() {
#    freedesktop-desktop_pkg_postinst
#}
#
#pkg_postrm() {
#    freedesktop-desktop_pkg_postrm
#}

